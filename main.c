// Created by Griffin Walraven
//   ___________        __
//  / ___/ ___\ \      / /
// | |  _\___ \\ \ /\ / / 
// | |_| |___) |\ V  V /  
//  \____|____/  \_/\_/   
//
// Chatbot that uses morse code.
//
// Base Build command:
// clang -lSDL2main -lSDL2_mixer -lSDL2 main.c
// With Extra stuff:
// clang -lSDL2main -lSDL2_mixer -lSDL2 -Werror -Wall -Wextra main.c
//
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>


//The music that will be played
Mix_Music *gMusic = NULL;

//The sound effects that will be used
Mix_Chunk *gDit = NULL;
Mix_Chunk *gDah = NULL;

char alphabet[26][1] = {
	"a",
	"b",
	"c",
	"d",
	"e",
	"f",
	"g",
	"h",
	"i",
	"j",
	"k",
	"l",
	"m",
	"n",
	"o",
	"p",
	"q",
	"r",
	"s",
	"t",
	"u",
	"v",
	"w",
	"x",
	"y",
	"z"
};

char alphabet_string[27] = "abcdefghijklmnopqrstuvwxyz ";

char morse_alphabet[27][5] = {
	".-",
	"-...",
	"-.-.",
	"-..",
	".",
	"..-.",
	"--.",
	"....",
	"..",
	".---",
	"-.-",
	".-..",
	"--",
	"-.",
	"---",
	".--.",
	"--.-",
	".-.",
	"...",
	"-",
	"..-",
	"...-",
	".--",
	"-..-",
	"-.--",
	"--..",
	" "
};

int strLength(char * string){
	// Count through string until you reach the end
	int i;
	for (i = 0; i[string] != '\0'; i++);
	return i-1;
}

void flushInp(){
	int c;
	
	// Iterates through stdin until it gets to the end
	while ((c = getchar()) != '\n' && c != EOF);
}

char * trimNewline(char * string){
	static char trimmed_string[128];
	char *rest = NULL;

	// Make a copy of the string to be trimmed and replace the \n with NULL
	strcpy(trimmed_string, string);
	strtok_r(trimmed_string, "\n", &rest);
	
	return trimmed_string;
}

char * getString(){
	static char string[128];

	// Get the user's input
	fgets(string, 128, stdin);
	
	// Get rid of the \n at the end of the string
	strcpy(string, trimNewline(string));

	// If the user entered more characters suppossed too, flush stdin
	char lastRead = string[strLength(string)];
	if (lastRead == '\n') {
		flushInp();
	}

	return string;
}

void playSound(char * sound) {
	if (strcmp(sound, "dit") == 0){
		// Play dit sound
		Mix_PlayChannel(-1, gDit, 0);
		printf(".");
	} else if (strcmp(sound, "dah") == 0){
		// Play dah sound
		Mix_PlayChannel(-1, gDah, 0);
		printf("-");
	} else if (strcmp(sound, " ") == 0){
		printf("   ");
	}
	
	// flush the output so it will display in real time
	fflush(stdout);
};

void listAlphabet(){
	// Iterate through the alphabet, printing each char and its morse code
	for (int i = 0; i < 26; i++){
		printf("%c : %s\n", alphabet_string[i], morse_alphabet[i]);
	}
}

char * lowerString(char * string){
	// Iterate through the given string and change each value to the lowecase equivalent
	for (int i = 0; i <= strLength(string); i++) string[i] = tolower(string[i]);
	return string;
}

int locationOf(char * string, char c){
	char *e;
	int index;
	
	// Find the location of the character in the alphabet string
	// This relates to the location of the characters morse code value
	// the index will be that location minus the location of the string itself
	e = strchr(string, c);
	index = (int)(e - string);

	return index;
}

void sayMorse(char * morse){
	char current;

	for (int i = 0; i <= strLength(morse); i++){
		current = morse[i];

		// Logic for sleeps:
		// dit is length 1, 125ms, dah is 3 times that, 375ms
		// space between parts of one character is length 1, 125ms
		// space between words, ' ', is length seven, however we already take 375ms
		// after every char so, (7 * 125ms) - 375ms = 500ms
		if (current == '.'){
			playSound("dit");
			// pause to play sound
			usleep(125000);

			// pause between parts of one character
			usleep(125000);
		} else if (current == '-'){
			playSound("dah");
			// pause to play sound
			usleep(375000);
			
			// pause between parts of one character
			usleep(125000);
		} else if (current == ' '){
			// pause between words, minus pause between characters that will be applied
			playSound(" ");
			usleep(500000);
		}

	}
}

void sayString(char * string_to_read){
	char current_char;
	int current_char_index;
	
	// Loop through the given string and say each character
	for (int i = 0; i <= strLength(string_to_read); i++){
		current_char = string_to_read[i];
		current_char_index = locationOf(alphabet_string, current_char);
		sayMorse(morse_alphabet[current_char_index]);
		
		// There is a pause of length 3 between characters, 125ms * 3 = 375ms
		// This is taken into account for a pause between words, ' '
		printf(" ");
		usleep(375000);
	}
	
	printf("\n");
}

int stringValid(char * string){
	int true = 1;

	for (int i = 0; i <= strLength(string) && true; i++ ){
		if (strchr(alphabet_string, string[i]) == NULL){
			true = 0;
		}	
	}

	return true;
}

void readString(){
	char string_to_read[128];
	char lowercase_string_to_read[128];
	
	// Get the string to read from the user
	printf("What would you like me to read?\n \" ");
	strcpy(string_to_read, getString());
		
	// Create a lowercase version of the string to compare to the alphabet
	// It also is needed because morse code doesn't support uppercase letters
	strcpy(lowercase_string_to_read, lowerString(string_to_read));
	
	// Make sure the string inlclude valid characters
	// If not, call this function to give the user another chance
	if (!stringValid(lowercase_string_to_read)){
		printf("You entered invalid characters, try again!\n");
		readString();
	} else {
		// Call the function to say the string
		sayString(string_to_read);
	}
}

int doAction(char * current_action){
		int cont = 1;	

		// Check to see what action the user wants to commit
		if (strcmp(current_action, "dit") == 0 || strcmp(current_action, "dah") == 0){
			
			// If the user just wants to play either the "dit" or "dah" sound, do so
			playSound(current_action);
			
		} else if (strcmp(current_action, "q") == 0){

			// If the user wants to quit, set the continuation variable to false
			cont = 0;

		} else if (strcmp(current_action, "list") == 0){

			// If the user wants to see what the morse code alphabet is, list it
			listAlphabet();

		} else if (strcmp(current_action, "read") == 0) {

			// If the user wants to have something read, call the readString() function
			// It will prompt the user for a string and then read it to him
			readString();
			
		
		} else if (strcmp(current_action, "help") == 0){
			
			// Give the user a list of all of the commands he can use
			printf("  dit  - Play a \"dit\" sound \n  dah  - Play a \"dah\" sound \n  q    - Quit the program \n  list - Print Morse Code cheat sheet \n  read - Have the computer read a string for you \n  help - Print this help screen out \n");
		} else {

			// If the user input something unrecognizable let him know, but don't exit
			printf("Hmm, I don't know that one. Try again, or use \"help\"\n");

		}
		
		return cont;
}

int initSDL() {
	int success = 1;

	//Initialize SDL
    if( SDL_Init (SDL_INIT_AUDIO ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = 0;
    }
	 //Initialize SDL_mixer
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
	{
		printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
		success = 0;
	}
	return success;
};

int loadMedia() {
	int success = 1;

	//Load music
    //gMusic = Mix_LoadMUS( "dit.wav" );
    //if( gMusic == NULL )
    //{
    //    printf( "Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError() );
    //    success = 0;
    //}

	//Load sound effects
    gDit = Mix_LoadWAV( "dit.wav" );
    if( gDit == NULL )
    {
        printf( "Failed to load dit sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
        success = 0;
    }
    
    gDah = Mix_LoadWAV( "dah.wav" );
    if( gDah == NULL )
    {
        printf( "Failed to load dah sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
        success = 0;
    }

	return success;
};

void closeSDL() {
    //Free the sound effects
    Mix_FreeChunk( gDit );
    Mix_FreeChunk( gDah );
    gDit = NULL;
    gDah = NULL;
    
    //Free the music
    Mix_FreeMusic( gMusic );
    gMusic = NULL;

    //Quit SDL subsystems
    Mix_Quit();
    SDL_Quit();
}

int main () {
	// some variables for basic communication
	static char current_action[64];
	int cont = 1;

	// Initialize sdl and sdl_mixer
	// Load the media files
	initSDL();
	loadMedia();

	while (cont){

		// Get an action from the user
		printf("What would you like to do?\n > ");
		strcpy(current_action, getString());
		
		// Do the action the user gave
		// The returned value says whether the user wants to quit or not
		cont = doAction(current_action);

	}	

	closeSDL();

	return 0;
}
