# Morse Code Communication

So I want to learn Morse code, and I figured the best way to learn is to simply use morse code. This program is designed to be a learning experience on two fronts, learn C and Morse code!

## Main Goal
The main goal of this is to have a simple chatbot that uses Morse code in order to communicate. 

## Usage

### Installation
The main.c includes a build command for the project. It was built on macOS, so if you are on a different OS it will probably be different. I use standard C libraries as well as SDL2 and SDL\_mixer for this project, so make sure those are properly installed and configured on your machine. If you are having trouble I would recommend looking at the official SDL docs (put "SDL documentation" into your favorite search engine).

### Interface
In its current state this is just a cli application. When first running the executable you will be presented with a command prompt very similar to what you might expect with bash, zsh, fish, etc.

### Commands

#### dit
NOTE: Soon to be removed.
Plays the programmed dit sound for reference.

#### dah
NOTE: Soon to be removed.
Plays the programmed dah sound for reference.

#### q
Quits the program (If stuck ^C works too ... just be careful as this is not recommened unless absolutely required).

#### list
This command will list out all the alphabet and each characters equivelent morse code, essentially just a cheat sheet.

#### read
This command invokes a secondary command prompt (noted by the '"'). You can give it a list of characters, also known as words, to read out in morse code. Currently it only supports the alphabet and spaces, no other characters. Please also note that upppercase and lowercase are ignored, as they aren't represented in standard morse code.

#### help
Self explanitory, list outs all the commands you can execute and gives a brief description of what each one does.


## TODO
- Add a morse definition for miscellaneous characters (such as . , ; : etc.).
- Add the "chat" part of the chatbot, instead of just command line style commands, includes:
	- Program some simple responses to certain commands (ie "hello" could returns "hi there!").
	- Program more complex commands that give the chatbot information about the user (such as names).
	- Program ever more complex responses that use the given information about the user.
	- Create some questions that the chatbot can ask the user if the user has nothing to say (maybe invoke with a command?).
	- Allow input via morse code (aka morse code detector and decoder)
	- Create a live window (rather than terminal input) so morse code input can be done with time rather than having to enter spaces between characters and words
- Allow input via morse code for command input as well
